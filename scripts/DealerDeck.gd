extends Control

onready var GameNode = get_node("..")
onready var PlayerControlsNode = get_node("../PlayerControls")

func _ready():
	GameNode.connect("displayNeedsUpdating", self, "updateDisplay")
	PlayerControlsNode.connect("displayNeedsUpdating", self, "updateDisplay")

func updateDisplay():
	if CardManager.isPlaying:
		$"DealerLabel".text = "Dealer"
	else:
		$"DealerLabel".text = "Dealer: " + str(CardManager.dealerCardsTotal)
		
	$"DealerScoreLabel".text = "Score: " + str(CardManager.dealerScore)

	for i in range(CardManager.dealerCards.size()):
		var card = CardManager.dealerCards[i]
		var nodeName = "CardPlaceholder" + str(i + 1)
		get_node(nodeName).visible = true
		# if first card, hide it but if game ended show it
		if card.isFirstCard && CardManager.isPlaying:
			get_node(nodeName + "/Card/Image").texture = load("res://cards/Back Covers/Pomegranate.png")
		else:
			get_node(nodeName + "/Card/Image").texture = load("res://cards/" + card.suit + "/" + card.rank + ".png")
