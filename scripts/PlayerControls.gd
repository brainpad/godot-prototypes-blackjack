extends Control
signal displayNeedsUpdating

func _process(delta):
	if CardManager.isPlaying && CardManager.isPlayerTurn:
		enableControls()
	else:
		disableControls()
		
func enableControls():
	$"HitButton".disabled = false
	$"StandButton".disabled = false
	
func disableControls():
	$"HitButton".disabled = true
	$"StandButton".disabled = true

func _on_RestartButton_pressed():
	# hide status message
	$"../StatusMessage".hide()
	# show controls
	$"HitButton".show()
	$"StandButton".show()
	$"..".newGame()

func _on_StandButton_pressed():
	CardManager.standPlayer()
	disableControls()
	emit_signal("displayNeedsUpdating")

func _on_HitButton_pressed():
	CardManager.hitPlayer()
	emit_signal("displayNeedsUpdating")
