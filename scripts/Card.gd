extends Control

class_name Card

var suit = null;
var rank = null;

var isFirstCard = false

func _init(newCardSuit, newCardRank):
	suit = newCardSuit
	rank = newCardRank
