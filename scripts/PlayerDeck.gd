extends Control

onready var GameNode = get_node("..")
onready var PlayerControlsNode = get_node("../PlayerControls")

func _ready():
	GameNode.connect("displayNeedsUpdating", self, "updateDisplay")
	PlayerControlsNode.connect("displayNeedsUpdating", self, "updateDisplay")

func updateDisplay():
	$"PlayerLabel".text = "Player: " + str(CardManager.playerCardsTotal)
	$"PlayerScoreLabel".text = "Score: " + str(CardManager.playerScore)

	for i in range(CardManager.playerCards.size()):
		var card = CardManager.playerCards[i]
		var nodeName = "CardPlaceholder" + str(i + 1)
		get_node(nodeName).visible = true
		get_node(nodeName + "/Card/Image").texture = load("res://cards/" + card.suit + "/" + card.rank + ".png")
