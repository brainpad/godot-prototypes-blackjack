extends Node
signal lookForWinnerOrNextTurn

var suits = [
	'Hearts',
	'Diamonds',
	'Spades',
	'Clubs'
	]

var ranks = [
	'Two',
	'Three',
	'Four',
	'Five',
	'Six',
	'Seven',
	'Eight',
	'Nine',
	'Ten',
	'Jack',
	'Queen',
	'King',
	'Ace'
	]

var values = {
	'Two'  : 2, 
	'Three': 3, 
	'Four' : 4, 
	'Five' : 5, 
	'Six'  : 6, 
	'Seven': 7, 
	'Eight': 8, 
	'Nine' : 9, 
	'Ten'  : 10, 
	'Jack' : 10, 
	'Queen': 10, 
	'King' : 10, 
	'Ace'  : 11
	}

var stack = []

var dealerCards = []
var playerCards = []

var dealerCardsTotal = 0
var playerCardsTotal = 0

var dealerScore = 0
var playerScore = 0

var isPlaying = false
var isPlayerTurn = false

var isDealerStanding = false
var isPlayerStanding = false

func dealerTurn():
	print("DEALER's' TURN:")
	#TODO: implement dealer AI
	if dealerCardsTotal < 17:
		print("dealer score is UNDER 17 so HIT")
		hitDealer()
	else:
		print("dealer score is OVER 17 so STAND")
		standDealer()
	
	print("PLAYER'S TURN:")
	isPlayerTurn = true

func reset():
	isPlaying = true
	isPlayerTurn = true
	isDealerStanding = false
	isPlayerStanding = false

# generate a new stack of 52 cards
func generateStack():
	for suit in suits:
		for rank in ranks:
			stack.append(Card.new(suit, rank))
	
# shuffle the current stack
func shuffleStack():
	randomize()
	stack.shuffle()

# pop one card off the current stack
func getCardFromStack():
	return stack.pop_front()

func clearDecks():
	dealerCards = []
	dealerCardsTotal = 0
	playerCards = []
	playerCardsTotal = 0

func distributeCards():
	# give dealer cards
	var card = getCardFromStack()
	# flag first to hide
	card.isFirstCard = true
	dealerCards.append(card)
	dealerCards.append(getCardFromStack())
	# give player cards
	playerCards.append(getCardFromStack())
	playerCards.append(getCardFromStack())

func standDealer():
	print("standDealer")
	isPlayerTurn = true
	isDealerStanding = true
	emit_signal("lookForWinnerOrNextTurn")

func standPlayer():
	print("standPlayer")
	isPlayerTurn = false
	isPlayerStanding = true
	emit_signal("lookForWinnerOrNextTurn")

func hitDealer():
	dealerCards.append(getCardFromStack())
	computeTotals()
	emit_signal("lookForWinnerOrNextTurn")
	
func hitPlayer():
	playerCards.append(getCardFromStack())
	computeTotals()
	isPlayerTurn = false
	emit_signal("lookForWinnerOrNextTurn")

func computeTotals():
	# compute dealer's total
	dealerCardsTotal = 0
	for card in dealerCards:
		var value = values[card.rank]
		if (dealerCardsTotal + value) > 21:
			value = 1
		dealerCardsTotal += values[card.rank]
		print("dealerCard: " + card.rank + " of " + card.suit + " (" + str(values[card.rank]) + ")")

	print("dealerCardsTotal: " + str(dealerCardsTotal))
	print("-")
	
	# compute player's total
	playerCardsTotal = 0
	for card in playerCards:
		var value = values[card.rank]
		if (playerCardsTotal + value) > 21:
			value = 1
		playerCardsTotal += values[card.rank]
		print("playerCard: " + card.rank + " of " + card.suit + " (" + str(values[card.rank]) + ")")

	print("playerCardsTotal: " + str(playerCardsTotal))
	print("-")
