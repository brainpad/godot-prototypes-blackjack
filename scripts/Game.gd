extends Control
signal displayNeedsUpdating

func _ready():
	CardManager.connect("lookForWinnerOrNextTurn", self, "lookForWinnerOrNextTurn")

func lookForWinnerOrNextTurn():
	# check if player busted
	if CardManager.playerCardsTotal > 21:
		CardManager.dealerScore += 1
		displayMessageAndStopGame("Player BUSTED!")

	# check if dealer busted
	elif CardManager.dealerCardsTotal > 21:
		CardManager.playerScore += 1
		displayMessageAndStopGame("Dealer BUSTED!")

	# TODO: if player has blackjack and dealer has blackjack it will never draw because of the order of these conditions
	# if somebody has 21: WINNER right away
	elif CardManager.playerCardsTotal == 21:
		CardManager.playerScore += 1
		displayMessageAndStopGame("Player has blackjack!")

	elif CardManager.dealerCardsTotal == 21:
		CardManager.dealerScore += 1
		displayMessageAndStopGame("Dealer has BlackJack!")
	
	# check if it's dealer's turn to play
	elif CardManager.isPlaying && !CardManager.isPlayerTurn && !CardManager.isDealerStanding:
		CardManager.dealerTurn()

	# if dealer can't play, check if player can
	elif CardManager.isPlaying && CardManager.isDealerStanding && !CardManager.isPlayerStanding:
		CardManager.isPlayerTurn = true

	# if both are standing, draw conclusion
	elif CardManager.isPlaying && CardManager.isDealerStanding && CardManager.isPlayerStanding:
		if CardManager.playerCardsTotal > CardManager.dealerCardsTotal:
			CardManager.playerScore += 1
			displayMessageAndStopGame("Player wins!")

		elif CardManager.dealerCardsTotal > CardManager.playerCardsTotal:
			CardManager.dealerScore += 1
			displayMessageAndStopGame("Dealer wins!")

	# TODO: what if they both get 21? it's a draw but they won't be both standing?
	elif (CardManager.playerCardsTotal != 0 && CardManager.dealerCardsTotal != 0) && (CardManager.isDealerStanding && CardManager.isPlayerStanding) && CardManager.playerCardsTotal == CardManager.dealerCardsTotal:
		displayMessageAndStopGame("Draw!")
	
func displayMessageAndStopGame(msg):
	$"StatusMessage".text = msg
	$"StatusMessage".visible = true
	CardManager.isPlaying = false

func newGame():
	# show labels
	$"DealerDeck/DealerLabel".show()
	$"PlayerDeck/PlayerLabel".show()
	$"DealerDeck/DealerScoreLabel".show()
	$"PlayerDeck/PlayerScoreLabel".show()
	# clear decks
	CardManager.clearDecks()

	# clear card placeholders
	for n in range(1, 9):
		get_node("PlayerDeck/CardPlaceholder" + str(n)).unsetCard()
		get_node("DealerDeck/CardPlaceholder" + str(n)).unsetCard()

	# create card stack
	CardManager.generateStack()
	# shuffle cards
	CardManager.shuffleStack()
	# reset card manager
	CardManager.reset()
	# distribute cards
	CardManager.distributeCards()
	# compute totals
	CardManager.computeTotals()
	# dealer turn
	lookForWinnerOrNextTurn()
	# update display
	emit_signal("displayNeedsUpdating")
